.PHONY: clean

PYTHON := /usr/bin/python
MDIOCRE := mdiocre/mdiocre.py

all: built

SRCFOLDER := src
SRCFILES := $(shell find $(SRCFOLDER)/ -type f | sed 's/ /\\ /g')

built: $(SRCFOLDER) $(SRCFILES)
	$(PYTHON) $(MDIOCRE) $< $@

clean:
	rm -r built
