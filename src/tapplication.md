<!--: mdiocre-template = '../template.html' -->

# the-libs Documentation

## tApplication

The  base  application  class, basically a wrapper around QApplication to add some extra stuff the About dialog can display, and then some other stuff.

## Classes

### tApplication (int &argc, char **argv)
Instantiates a tApplication.

## Slots

### void setApplicationIcon (QIcon icon)

Sets the current application's window icon to `icon`.

### void setGenericName (QString genericName)

Used  in  tAboutDialog.  Sets the current application's generic name to genericName. The application's generic name would be a general description of the application in the form of, e.g. "Firefox is a (web browser)".
