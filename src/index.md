<!--: mdiocre-template = '../template.html' -->

# theDocs

## Introduction

`the-libs` is a library providing common functionality to theDesk and theShell applications (otherwise known as the\* apps or theSuite) such as application settings, custom widgets, toasts, among others. Most of these subclass existing Qt functionalities and extend them, however some also provide new and easier interfaces e.g. [tPromise](tpromise.html)

## List of libraries
* [tApplication](tapplication.html)
* [tPromise](tpromise.html)

## Lorem Ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque lacinia turpis quis laoreet hendrerit. Morbi lacinia posuere augue sit amet finibus. Nunc vestibulum egestas purus a volutpat. Phasellus venenatis egestas elit, eget interdum sapien dapibus sed. Nam mollis, erat euismod convallis consequat, quam risus sagittis tortor, eu imperdiet urna libero quis elit. Sed posuere velit quis erat hendrerit malesuada nec at erat. Duis cursus posuere sapien, in ornare elit pharetra at. Morbi eu varius massa. Etiam ornare mollis molestie. Curabitur volutpat viverra urna, sed aliquam urna fermentum sed. Suspendisse potenti. Nullam sed semper risus.

Maecenas ultricies convallis blandit. In pellentesque posuere ipsum, ac varius libero cursus ornare. Nunc nec consectetur tortor, vel suscipit nisl. Aliquam tristique hendrerit nulla, ut tristique dui. Suspendisse elementum leo ut fermentum finibus. Morbi enim enim, pulvinar id tincidunt non, blandit et magna. Ut vehicula purus in pharetra aliquam. Duis eu purus metus. Suspendisse potenti. Phasellus lacinia ultricies tristique. Aliquam placerat magna sed dignissim scelerisque. Vestibulum tincidunt aliquet eros, et interdum metus efficitur id. Nam nec tincidunt risus, non elementum diam.

Sed condimentum ornare mi. Etiam posuere neque nec mauris eleifend pharetra. Praesent pharetra blandit dui in venenatis. Nulla arcu nisl, vestibulum vel nulla eu, aliquet aliquet erat. Etiam porta, dui ut lacinia blandit, nisl odio ultricies ante, egestas porttitor nibh lacus at libero. Curabitur magna odio, blandit sed venenatis nec, auctor sit amet dolor. Quisque non turpis vitae odio euismod tincidunt ac quis felis. Aliquam erat volutpat. Curabitur vel gravida tortor. Fusce dictum ut risus quis tempor. Quisque pellentesque mi tellus, vel sollicitudin metus scelerisque nec.

Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla eros arcu, euismod a felis quis, pharetra placerat nisl. Morbi blandit libero gravida quam lobortis, ac molestie ligula consequat. Morbi id quam mollis, feugiat magna ut, pellentesque ex. Integer at aliquam nisi. In rhoncus vehicula quam ut elementum. Proin bibendum diam ac velit dignissim, vitae molestie ante euismod. Nulla ut lacus erat. Vivamus ac blandit lectus. Proin bibendum vel mi id faucibus. Donec a placerat metus, eu dapibus tortor.

Fusce pharetra libero et odio semper viverra. Donec quis viverra risus, eget iaculis mi. Suspendisse laoreet est quis fermentum fermentum. Nullam ac ipsum vel nunc dignissim vestibulum. Morbi viverra interdum scelerisque. Integer lobortis tempus viverra. Quisque sollicitudin lobortis libero, et venenatis nulla pharetra a. Morbi ut ex non mauris placerat fringilla quis id nulla. Aenean a arcu ac ligula tempus mollis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam at eros lacus. Fusce fermentum aliquam malesuada. Aenean convallis nunc metus, quis cursus felis mollis quis. Duis nisi orci, ornare nec quam ut, vulputate volutpat dui. Proin at vestibulum erat.
